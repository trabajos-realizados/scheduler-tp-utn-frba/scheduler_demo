;======================================================================;
; Rutina: uint32_t strlen(char *str):                                  ;
; Devuelve el largo de la cadena                                       ;
;======================================================================;
strlen:
    push ebp        ;PROLOGO
    mov ebp, esp
    push edi        ;CUERPO
    
    mov eax, 0
    mov edi, dword[ebp + 8]
.bucle:
    cmp byte[eax+edi], 0
    je fin_strlen
    inc eax         ;incremento
    jmp .bucle
fin_strlen:
    pop edi
    mov esp, ebp
    pop ebp
    ret
    
;======================================================================;
; Rutina: void itoa(unsigned uint32_t num, char *str):                 ;
; Convierte un uint32_t a ASCII en base 16 (hexadecimal)               ;
;======================================================================;
itoa:
    push ebp
    mov ebp, esp
    sub esp, 4  ;Reservo 4B en la pila.
    
    push eax
    push ebx
    push ecx
    push edx
    push edi
    
    mov edi, 0
    mov eax, dword[ebp+8]   ;num ECX:EAX, DW Bajo
    mov ebx, dword[ebp+12]  ;Tomo str.
conversion_DW:
    mov edx, 0
    mov ecx, 16
    div ecx                  ;EDX:EAX/16, el resto esta en EDX.    
    
    cmp edx, 9
    ja .letra                ;Si EDX>9 salto a "letra".
    add dl, '0'
    jmp guardar_DW
.letra:
    add dl, 'A'-10
guardar_DW:
    mov byte[ebx+edi], dl   ;Guardo caracter en el *str.
    inc edi                 ;EDI++
    cmp edi, 8              ;Si EDI=8 significa que se analizaron los 8 nibles del DWORD de num.
    jne conversion_DW       ;Si no es igual continuo con la canversión.
    
    mov ecx, 8
.limpiar_ceros:              ;Los '0' no significativos con reemplazados por '\0'.
    mov byte[ebx+ecx], 0     ;Agrego '\0'
    cmp byte[ebx+ecx-1], '0'
    jne .invertir_string     ;Si es distinto de '0', entonces salto a invertir_string.    
    loop .limpiar_ceros
.invertir_string:
    push ebx
    call strlen     ;Devuelve en EAX el tamaño del string.
    add esp, 4
    mov ecx, eax
    sub eax, 1
    mov edi, 0
    shr ecx, 1      ;Divido entre 2 ECX.
    mov dword[ebp-8], ecx   ;Guardo ECX en la pila.
.loop_inversion:
    mov esi, eax
    sub esi, edi
    mov dl, byte[ebx+esi]   ;Salvo caracter en DL
    mov cl, byte[ebx+edi]
    mov byte[ebx+esi], cl
    mov byte[ebx+edi], dl   ;
    inc edi
    cmp edi, dword[ebp-8]   ;Si EDI<ECX continuo con la inversion, 
    jb .loop_inversion       ;Si no, finalizo la subrutina.
    
    mov esp, ebp
    pop ebp
    ret
fin_itoa:
    
;======================================================================;
; Rutina: void itoa64(unsigned uint64_t num, char *str):               ;
; Convierte un uint64_t a ASCII en base 16 (hexadecimal)               ;
;======================================================================;
itoa64:
    push ebp
    mov ebp, esp
    sub esp, 4  ;Reservo 4B en la pila.
    
    push eax
    push ebx
    push ecx
    push edx
    push edi
    
    mov edi, 0
    mov eax, dword[ebp+8]   ;num ECX:EAX, DW Bajo
    mov ebx, dword[ebp+16]  ;Tomo str.
conversion_DW_L:
    mov edx, 0
    mov ecx, 16
    div ecx                  ;EDX:EAX/16, el resto esta en EDX.    
    
    cmp edx, 9
    ja .letra                ;Si EDX>9 salto a "letra".
    add dl, '0'
    jmp guardar_DW_L
.letra:
    add dl, 'A'-10
guardar_DW_L:
    mov byte[ebx+edi], dl   ;Guardo caracter en el *str.
    inc edi                 ;EDI++
    cmp edi, 8              ;Si EDI=8 significa que se analizaron los 8 nibles del DWORD BAJO de num.
    je continuar            ;Analizo el DWORD ALTO de num.
    jmp conversion_DW_L     ;Si no, sigo converios de la parte baja del numero.
    
continuar:
    mov eax, dword[ebp+12]   ;DW ALTO de num.
conversion_DW_H:
    mov edx, 0
    mov ecx, 16
    div ecx                  ;EDX:EAX/16, el resto esta en EDX.    
    
    cmp edx, 9
    ja .letra                ;Si EDX>9 salto a "letra".
    add edx, '0'
    jmp guardar_DW_H
.letra:
    add edx, 'A'-10
guardar_DW_H:
    mov byte[ebx+edi], dl   ;Guardo caracter en el *str.
    inc edi                 ;EDI++
    cmp edi, 16             ;Si EDI=16 significa que se analizaron los 16 nibles del DWORD ALTO de num.
    jne conversion_DW_H     ;Si no es igual, sigo la conversión.
    
    mov ecx, 16
limpiar_ceros:              ;Los '0' no significativos con reemplazados por '\0'.
    mov byte[ebx+ecx], 0    ;Agrego '\0'
    cmp byte[ebx+ecx-1], '0'
    jne invertir_string     ;Si es distinto de '0', entonces saldo a invertir_string.    
    loop limpiar_ceros
invertir_string:
    push ebx
    call strlen     ;Devuelve en EAX el tamaño del string.
    add esp, 4
    mov ecx, eax
    sub eax, 1
    mov edi, 0
    shr ecx, 1      ;Divido entre 2 ECX.
    mov dword[ebp-8], ecx   ;Guardo ECX en la pila.
loop_inversion:
    mov esi, eax
    sub esi, edi
    mov dl, byte[ebx+esi]   ;Salvo caracter en DL
    mov cl, byte[ebx+edi]
    mov byte[ebx+esi], cl
    mov byte[ebx+edi], dl   
    inc edi
    cmp edi, dword[ebp-8]   ;Si EDI<ECX continuo con la inversion, 
    jb loop_inversion       ;Si no, finalizo la subrutina.
    
    mov esp, ebp
    pop ebp
    ret
fin_itoa_x64:
