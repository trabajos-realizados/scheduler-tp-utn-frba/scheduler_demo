extern __page_tables

;***********************************************
;Direcciones de paginas
DIR_PAG_1       EQU __page_tables
TABLA_PAG_1a    EQU DIR_PAG_1 + 0x1000
TABLA_PAG_1b    EQU TABLA_PAG_1a + 0x1000

DIR_PAG_2       EQU TABLA_PAG_1b + 0x1000
TABLA_PAG_2a    EQU DIR_PAG_2 + 0x1000
TABLA_PAG_2b    EQU TABLA_PAG_2a + 0x1000
TABLA_PAG_2c    EQU TABLA_PAG_2b + 0x1000

DIR_PAG_3       EQU TABLA_PAG_2c + 0x1000
TABLA_PAG_3a    EQU DIR_PAG_3 + 0x1000
TABLA_PAG_3b    EQU TABLA_PAG_3a + 0x1000
TABLA_PAG_3c    EQU TABLA_PAG_3b + 0x1000

DIR_PAG_4       EQU TABLA_PAG_3c + 0x1000
TABLA_PAG_4a    EQU DIR_PAG_4 + 0x1000
TABLA_PAG_4b    EQU TABLA_PAG_4a + 0x1000
TABLA_PAG_4c    EQU TABLA_PAG_4b + 0x1000

SIZE_PAGE_TABLES    EQU (TABLA_PAG_4b-DIR_PAG_1+0x1000)
SIZE_ENTRY_PAGE     EQU (SIZE_PAGE_TABLES/4)

;***********************************************
;Atributos
PS          EQU 0x80    ;Page Size
SUP         EQU 0x00    ;Super user
USR         EQU 0x04    ;User
WE          EQU 0x02    ;Write enable
P           EQU 0x01    ;Present
