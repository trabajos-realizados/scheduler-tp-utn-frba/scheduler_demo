;Tareas
extern Tarea_1

;Direcciones físicas
extern __tabla_digitos

;Funciones ASM
extern Conversion
extern ImprimirPantalla
extern strlen

extern td3_halt
extern sel_sysCall_halt
extern sel_sysCall_read
extern sel_sysCall_print
extern sel_sysCall_cantidadTablaDigitos
extern sel_sysCall_readTablaDigitos
extern sel_sysCall_imprimirPantalla

;Funciones C
extern initCircularBuffer
extern writeCircularBuffer
extern readCircularBuffer
extern itoa

;Variables globales
global TSS_inicial
global contexto_pila
global tarea_actual
global tarea_futura
global estado_halted
global contexto_simd

global cont
global flag_enter
global timer_tick
global tick_tarea_1
global tick_tarea_2
global tick_tarea_3
global buffer
global index_carga
global string_k
global pages

global variable_generica

;*****************************************************************************;
section .bss    NOBITS
align 512
contexto_simd   resb    512*4
TSS_inicial     resb    0x68    ;104B de TSS
contexto_pila   resb    8*5     ;Contexto de pila kernel y de las 4 tareas: [SSn:ESPn].
tarea_actual    resb    1       ;Guarda el numero de la tarea en curso (1, 2, 3 o 4)
estado_halted   resb    1       ;Se pone en 1 cuando la tarea esta en estado HLT, de lo contrario permance en 0.
                                
buffer          resb (17+2*4+1) ;17B de datos, 2 dword de punteros mas 1B del contador.
flag_enter      resb 1
timer_tick      resb 1
tick_tarea_1    resb 1
tick_tarea_2    resb 1
tick_tarea_3    resb 1
timer_cont      resb 2          ;Contador de timer
flag_carga      resb 1
index_carga     resb 2
string_k        resb 36         ;string usado en funciones de kernel
pages           resb 1          ;Reserva la cantidad de paginas, utilizado en Page Fault

variable_generica   resq    1   ;Variable generica para leer con td3_read();

;*****************************************************************************;
section .kernel PROGBITS
main_init:
    mov byte[flag_enter], 0    ;flag_enter=0
    mov byte[timer_tick], 0
    mov byte[tick_tarea_1], 0
    mov byte[tick_tarea_2], 0
    mov byte[tick_tarea_3], 0
    mov word[timer_cont], 0
    mov word[index_carga], 0
    mov byte[pages], 0
    
    mov byte[tarea_actual], 0       ;Tarea_actual=ninguna
    mov byte[estado_halted], 0x1    ;Estado Halted todos a uno.
    
    mov dword[variable_generica], '0123'        ;inicializo la variable_generica
    mov dword[variable_generica+4], '4567'
    mov dword[variable_generica+8], '89AB'
    mov dword[variable_generica+12], 'CDEF'
    
    push buffer
    call initCircularBuffer
    add esp, 4
    
programa:
    hlt
    
EventoTimerDato:

    cmp byte[timer_tick], 10    ;Cada 100ms incremento timer_cont.
    ;jne AnalisisFlagEnter       ;Si no pasaron 100ms analizo el buffer.
    ;inc word[timer_cont]        ;timer_cont++
    ;mov byte[timer_tick], 0

    jb AnalisisFlagEnter        ;Si no pasaron 100ms analizo el buffer.
    xor edx, edx
    movzx ax, byte[timer_tick]
    mov bx, 10
    div bx                      ;AX/10 => AL:resultado; AH:resto
    add byte[timer_cont], al    ;timer_cont++
    mov byte[timer_tick], ah    ;El resto se guarda en timer tick
    
AnalisisFlagEnter:
    cmp byte [flag_enter], 1    ;Detecto si el buffer esta listo para pasar a la tabla de datos
    jne programa

    mov byte [flag_enter], 0    ;Si está liso, limpio flag
    mov bl, 0                   ;Registro contador
    mov ebx, 0                  ;inicializo valores para usar en "cargarDatosTabla"
    mov ecx, 0
    mov byte[flag_carga], 0     ;Se usa para indicar si se estaba cargando en la tabla de datos.
cargarDatosTabla:
    push buffer
    call readCircularBuffer     ;Leo el buffer y el retorno se guarda en AL
    add esp, 4
    cmp al, 0x1C                ;'\r'
    jne .continuar
    cmp byte[flag_carga], 1     ;Si se estaba cargando en la tabla se incrementa EDI
    jne programa
    movzx edi, word[index_carga]
    mov dword[__tabla_digitos+edi*8], ebx   ;cargo en los primeros 32b
    mov dword[__tabla_digitos+edi*8+4], ecx ;cargo en los ultimos 32b
    add word[index_carga], 1    ;Se indexa a los siguientes 64 bits de la tabla.
    mov byte[flag_carga], 0
    jmp programa
.continuar:
    mov byte[flag_carga],1  ;Indico que se estan cargando datos.
    push eax
    call Conversion         ;Convierte el scan code a int y lo guarda en AL
    add esp, 4
;Para cargar los 64 bits en la tabla de datos, se utiliza los registros ECX:EBX
    shl ecx, 4
    push ebx                ;Salvo EBX en la pila.
    and ebx, 0xF0000000     ;Tomo los ultimos 4 bits
    shr ebx, 28             ;y los desplazo a los 4 primeros bits.
    add ecx, ebx            ;Muevo el resultado en EBX que son los ultimos 32bits
    pop ebx                 ;Recupero EBX
    shl ebx, 4
    add bl, al              ;Guardo el valor convertido en ebx
    jmp cargarDatosTabla
    
main_end:
