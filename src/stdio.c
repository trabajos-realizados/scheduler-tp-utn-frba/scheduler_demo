typedef unsigned char       uint8_t;
typedef unsigned short int  uint16_t;
typedef unsigned int        uint32_t;
typedef unsigned long long  uint64_t;

/* Convierte un numero entero de 32bits a ascii hexadecimal.
 * @uint32_t num:   numero a convertir
 * @char *str:      destino de la conversión
 * @NOTA:           No detecta signos ni bases.
 */
void itoa(uint32_t num, char *str)
{
    uint32_t resto, i=0, largo;
    char dig;
    
    do
    {
        resto=num%16;
        if(resto<=9)
            *(str+i)=resto+'0'; //Convierto a ascii
        else
            *(str+i)=resto+'A'-10; //Convierto a ascii
        num/=16;
        i++;
    }while(num!=0);
    
    largo=i;
    i=0;
    *(str+largo)='\0';
    while(i!=largo/2)       //Ordeno la cadena ya que esta invertida.
    {
        dig=*(str+i);
        *(str+i)=*(str+largo-1-i);
        *(str+largo-1-i)=dig;
        i++;
    }
}

/* Convierte un numero entero de 64 bits a ascii hexadecimal.
 * uint16_t num:    numero a convertir
 * @char *str:  destino de la conversión
 * @NOTA:       No detecta signos ni bases.
 */
void itoa64(uint64_t num, char *str)
{
    uint64_t resto, i=0, largo;
    char dig;
    
    do
    {
        resto=num%16;
        if(resto<=9)
            *(str+i)=resto+'0'; //Convierto a ascii
        else
            *(str+i)=resto+'A'-10; //Convierto a ascii
        num/=16;
        i++;
    }while(num!=0);
    
    largo=i;
    i=0;
    *(str+largo)='\0';
    while(i!=largo/2)       //Ordeno la cadena ya que esta invertida.
    {
        dig=*(str+i);
        *(str+i)=*(str+largo-1-i);
        *(str+largo-1-i)=dig;
        i++;
    }
}
