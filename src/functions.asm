;VARIABLES
extern tarea_actual
extern estado_halted
extern TSS_inicial
extern contexto_pila
extern contexto_simd
extern index_carga

extern flag_enter
extern timer_tick
extern tick_tarea_1
extern tick_tarea_2
extern tick_tarea_3
extern buffer
extern string_k
extern pages

extern variable_generica

;Direcciones
extern __video
extern __tabla_digitos

;FUNCIONES
extern writeCircularBuffer

;TABLAS
extern ds_stack 
extern imagen_GDT

;Funciones ASM
global ManejadorExc6
global ManejadorExc7
global ManejadorExc8
global ManejadorExc12
global ManejadorExc14
global ManejadorExc17
global ManejadorTeclado
global ManejadorTimer
global Conversion
global ImprimirPantalla
global td3_halt
global td3_print
global td3_read
global cantidadTablaDigitos
global readTablaDigitos
global read_Dir

;======================================================================;
;   INCLUDES                                                           ;
;======================================================================;
    %include "./inc/paginacion.inc"

;======================================================================;
;   SECTION RUTINAS                                                    ;
;======================================================================;
    section .rutinas PROGBITS
Rutinas:
;======================================================================;
; Rutina ImprimirPantalla: Imprime en pantalla lo enviado por argumento;
; ImprimirPantalla(int8 x, int8 y, char *str, int atributos)           ;
; Atributos: 1B [blink, R, G, B(fondo), brillo, R, G, B(frente)]       ;
;======================================================================;
ImprimirPantalla:      ;[es:di] Dir. Fis. VGA = 0xB8000
    push ebp        ;PROLOGO
    mov ebp, esp
                            ;CUERPO
    push ebp                ;Salvo el viejo valor de EBP.
    mov ebp, dword[ebp+12]  ;Tomo ESP de la pila.

    push dword[ebp+12]      ;atributo
    push dword[ebp+8]       ;*str
    push dword[ebp+4]       ;y
    push dword[ebp]         ;x
    call ImprimirKernel
    add esp, 4*4
    
fin_imprimirPantalla:
    pop ebp
    mov esp, ebp    ;EPILOGO
    pop ebp
    retf

;======================================================================;
; Rutina ImprimirKernel: Imprime en pantalla lo enviado por argumento  ;
; ImprimirKernel(int8 x, int8 y, char *str, int atributos)             ;  
; Atributos: 1B [blink, R, G, B(fondo), brillo, R, G, B(frente)]       ;
;======================================================================;
ImprimirKernel:      ;[es:di] Dir. Fis. VGA = 0xB8000
    push ebp        ;PROLOGO
    mov ebp, esp
    push eax        ;CUERPO
    push ebx
    push ecx
    push edx
    
    mov ebx, [ebp+8]            ;ebx<-x
    lea ebx, [__video+2*ebx]    ;ebx<-Dirección efectiva
    mov eax, [ebp+8+4]          ;eax<-y
    mov ecx, 160
    mul ecx                     ;edx:eax=160*eax(y)
    lea ebx, [ebx+eax]          ;ebx<-Dirección efectiva
    mov byte dh, [ebp+4*3+8]    ;ecx<-atributo
    mov eax, [ebp+8+4*2]        ;eax<-*str
    mov edi, 0
    
bucle_print:
    mov byte dl,[eax+edi]       ;dl<-mem[str+index] caracter en indice edi
    cmp dl,0                    ;Verifico que halla llegado a '\0'
    je fin_ImprimirKernel
    mov [ebx+edi*2], dx         ;Cargo caracter y atributos
    add edi, 1
    jmp bucle_print
fin_ImprimirKernel:
    pop edx
    pop ecx
    pop ebx
    pop eax
    mov esp, ebp    ;EPILOGO
    pop ebp
    ret

;======================================================================;
; Rutina conversión: Toma el scan code y lo convierte a uint8_t (al)   ;
;======================================================================;
Conversion:
    push ebp                ;PROLOGO
    mov ebp, esp
    
    mov al,[ebp+8]          ;CUERPO
    cmp al,0x0B             ; al='0'?
    jne numero
    mov al,0
    jmp fin_conversion
numero:
    mov byte al,[ebp+8]
    sub al, 1
fin_conversion:
    mov esp, ebp            ;EPILOGO
    pop ebp
    ret

;======================================================================;
; Rutina conversión: Toma el scan code y lo convierte a uint8_t (al)   ;
;======================================================================;
Asignar_pagina:
    push eax
    push ebx
    push ecx
    push edx
    push edi
    
    mov eax, cr2                ;Tomo la direccion lineal que causo el error.
    shr eax, 22                 ;EAX: indice de directorio de paginas.
    mov ecx, cr2
    shr ecx, 12
    and ecx, 0x3FF              ;ECX: Indice de tablas de paginas.
    mov ebx, cr3                ;EBX: Direccion base de paginacion   

    lea edx, [ebx+0x1003]       ;Direccion base de tablas de paginas.
    mov dword[ebx+eax*4], edx   ;Cargo directorio
    movzx eax, byte[pages]
    xor edx, edx
    mov edi, 0x1000
    mul edi                     ;EAX=EAX*0x1000  
    lea edx, [eax+0x0C000003]   ;Atributos: SUP+WE+P
    mov dword[ebx+ecx*4+0x1000], edx    ;Cargo tabla de paginas
    inc byte[pages]
      
    pop edi
    pop edx
    pop ecx
    pop ebx
    pop eax
    
    ret
    
;======================================================================;
; Manejador de compuerta de llamada: void td3_halt(void);              ;
;======================================================================;
td3_halt:    
    mov byte[estado_halted], 1  ;Aviso al sistema que alguna tarea estara en estado halt.
    hlt
    mov byte[estado_halted], 0  ;Aviso al sistema finalizo el estado halt.
    retf                        ;Retorno lejano
fin_td3_halt:

;===========================================================================;
; Manejador de compuerta de llamada:                                        ;
; unsigned int td3_read(void *buffer, unsigned int num_bytes);              ;
; Lee una variable genérica llamada variable_generica=0x0123456789ABCDEF.   ;
;===========================================================================;
td3_read:
    push ebp                    ;PROLOGO
    mov ebp, esp
    push edx                    ;CUERPO
    push ecx
    push ebx
    
    mov ebx, dword[ebp+12]      ;Tomo el ESP guardado en la pila de PL0.
    mov esi, dword[ebx]         ;ESI=buffer
    mov ecx, dword[ebx+4]       ;ECX=num_bytes
    xor eax, eax                ;EAX=0
cicloLectura:                   ;Guardo en el buffer, los bytes solicitados por el usuario.
    mov dl, byte[variable_generica+eax]
    mov byte[esi+eax], dl
    inc eax                     ;EAX++.
    loop cicloLectura
    pop ebx
    pop ecx
    pop edx
    mov esp, ebp                ;EPILOGO
    pop ebp
    retf
fin_td3_read:

;===========================================================================;
; Manejador de compuerta de llamada:                                        ;
; unsigned int td3_print(void *buffer, unsigned int num_bytes);             ;
; Imprime en la parte superior izquierda el buffer.                         ;
;===========================================================================;
td3_print:
    push ebp                    ;PROLOGO
    mov ebp, esp
    push edx                    ;CUERPO
    push ecx
    push ebx
    
printBuff:
    mov ebx, dword[ebp+12]      ;Tomo el ESP guardado en la pila de PL0.
    mov esi, dword[ebx]         ;ESI=buffer
    mov ecx, dword[ebx+4]       ;ECX=num_bytes
    xor eax, eax                ;EAX=0
    mov dh, 0x8F                ;Atributo
cicloPrint:
    mov dl, byte[esi+eax]
    mov word[__video+eax*2+160], dx
    inc eax                     ;EAX++.
    loop cicloPrint

    pop ebx
    pop ecx
    pop edx
    mov esp, ebp                ;EPILOGO
    pop ebp
    retf
fin_td3_print:
   
;===========================================================================;
; uint16_t cantidadTablaDigitos()                                           ;
; @ Nota: Devuelve la cantidad de datos guardados en la Tabla de Dígitos.   ;
; @ Return: uint16_t(AX) la cantidad de datos guardados en la Tabla.        ;
;===========================================================================;
cantidadTablaDigitos:
    mov ax, word[index_carga] ;Guardo en AX la cantidad de datos guardados en la tabla.
    retf
fin_cantidadTablaDigitos:
   
;===========================================================================;
; uint64_t readTablaDigitos(uint32_t indice)                                ;
; @ uint32_t indice: Corresponde al índice de la tabla a leer.              ;
; @ Return: uint64_t(EDX:EAX) valor de la tabla especificado por @indice.   ;
;===========================================================================;
readTablaDigitos:
    push ebp
    mov ebp, esp
    
    push ebx
    push edi
    
    mov ebx, dword[ebp+8+4]         ;Tomo el ESP guardado en la pila.
    mov edi, dword[ebx]             ;EDI=indice
    mov eax, dword[__tabla_digitos+8*edi]   ;EAX=dword bajo
    mov edx, dword[__tabla_digitos+8*edi+4] ;EDX=dword alto
    
    pop edi
    pop ebx
    pop ebp
    retf
fin_readTablaDigitos:

;===========================================================================;
; uint8_t read_Dir(uint32_t base)                                           ;
; @ uint32_t base: Direccion lineal donde se pretende leer.                 ;
; @ Return uint8_t (AL): Si se pudo efectuar la lectura devuelve un numero  ;
;                         de 8 bits.                                        ;
;===========================================================================;
read_Dir:
    push ebp
    mov ebp, esp
    
    push ebx
    push edi
    
    mov eax, 0
    mov ebx, dword[ebp+8+4] ;Tomo el ESP guardado en la pila.
    mov edi, dword[ebx]     ;EDI=base.
    movzx eax, byte[edi]    ;Leo.
fin_read_Dir:
    pop edi
    pop ebx
    pop ebp
    retf
    
RUTINAS_ISR:
;======================================================================;
; Manejador de interrupciones del timer.                               ;
; Scheduler                                                            ;
; Tarea 1: 500ms    Tarea 2: 100ms  Tarea 3: 200ms  Tarea 4: HALT      ;
;======================================================================;
ManejadorTimer:    
    PUSHAD              ;Salvo los registros de uso general.
    MOV AL,0x20         ;Envío End of Interrupt al PIC.
    OUT 0x20,AL

    inc byte[timer_tick]            ;timer_tick++.
    inc byte[tick_tarea_1]          ;tick_tarea_1++.
    inc byte[tick_tarea_2]          ;tick_tarea_2++.
    inc byte[tick_tarea_3]          ;tick_tarea_3++.

testTarea1: ;Se ejecuta cada 500mS
    cmp byte[tick_tarea_1], 50  ;Si tick < 50 salto a testTarea2, de lo contrario ejecuto Tarea 1
    jb testTarea2
    mov byte[tick_tarea_1], 0
    mov edx, DIR_PAG_1
    mov ebx, 1                  ;EBX: tarea futura
    jmp cambio_de_tarea
    
testTarea2: ;Se ejecuta cada 100mS
    cmp byte[tick_tarea_2], 10  ;Si tick < 10 salto a testTarea3, de lo contrario ejecuto Tarea 2
    jb testTarea3
    mov byte[tick_tarea_2], 0
    mov edx, DIR_PAG_2
    mov ebx, 2                  ;EBX: tarea futura
    jmp cambio_de_tarea
    
testTarea3: ;Se ejecuta cada 200mS
    cmp byte[tick_tarea_3], 20  ;Si tick < 20 salto a testTarea4, de lo contrario ejecuto Tarea 3
    jb testTarea4
    mov byte[tick_tarea_3], 0
    mov edx, DIR_PAG_3
    mov ebx, 3                  ;EBX: tarea futura
    jmp cambio_de_tarea
    
testTarea4:
    ;Antes de ejecutar la Tarea 4 "idle", se debe asegurar si la tarea actual establecio el modo HALT
    cmp byte[estado_halted], 0  ;Si es igual a 0 implica que la tarea esta ejecutandose, termino el manejador para continuar con la tarea.
    je fin_ManejadorTimer       ;Si esta en 1 (modo halt) ejecuto la tarea 4/Kernel
    mov edx, DIR_PAG_4
    cmp byte[tarea_actual], 0
    je runTarea4                ;Si es la estaba corriendo la Tarea 4, cambio al Kernel y viceversa.
runKernel:
    mov ebx, 0                  ;EBX: tarea futura
    jmp cambio_de_tarea
runTarea4:
    mov ebx, 4                  ;EBX: tarea futura
    
cambio_de_tarea:
    movzx eax, byte[tarea_actual]   ;EAX tiene el identificador de la tarea en curso.
    mov byte[tarea_actual], bl      ;Se actualiza tarea_actual.
guardarContexto:
    push gs                                 ;Salvo los registro de segmentos
    push fs
    push es
    push ds
    push dword[TSS_inicial+8]               ;Salvo SS0
    push dword[TSS_inicial+4]               ;Salvo ESP0
    mov word[contexto_pila+eax*8], ss       ;Salvo SS actual en el array.
    mov dword[contexto_pila+eax*8+2], esp   ;Salvo ESP actual en el array.
    mov cr3, edx                            ;Cargo el directorio de la nueva tarea.
    mov dx, word[contexto_pila+ebx*8]       ;Recupero SS de la tarea nueva del array.
    mov ss, dx
    mov esp, dword[contexto_pila+ebx*8+2]   ;Recupero ESP de la tarea nueva del array.
    pop dword[TSS_inicial+4]                ;Recupero ESP0
    pop dword[TSS_inicial+8]                ;Recupero SS0
    pop ds                                  ;Recupero los registros de segmentación
    pop es
    pop fs
    pop gs
    
simd:
    mov ebx, cr0
    bt ebx, 3
    jc taskSwitched         ;Consulto el bit 3 (TS), si esta en 1 salto a taskSwitched. Sino guardo contexto de SIMD.
    mov edx, 512
    mul edx                 ;Multiplico EDX*EAX para obtener el offset correspondiente a la tarea vieja y asi guardar el contexto.
    fxsave [contexto_simd+eax]  ;Guardo contexto.
taskSwitched:               ;Indico que hubo cambio de tarea.
    mov eax, cr0
    or eax, 0x8     ;       CR0.TS=1 (bit 3)
    mov cr0, eax
    
fin_ManejadorTimer:
    POPAD               ;Restauro registros de uso general.
    IRET                ;Fin de la interrupción.

;======================================================================;
; Manejador de interrupciones del teclado                              ;
;======================================================================;
ManejadorTeclado:
    pushad              ;Salvo los registros de uso general.
    MOV AL,0x20         ;Envío End of Interrupt al PIC.
    OUT 0x20,AL
    
    in al, 0x60         ;Leer tecla del buffer de teclado
    and al, al          ;Se comprueba si se soltó o no la tecla si el bit 7 esta en 1 o 0. Se consulta el flag de signo.
    js fin_int33        ;Terminar si se suelta la tecla.

;uint8_t writeCircularBuffer(CircularBuffer *circular_buffer, char info)
escribirBuffer:
    push eax                    ;Parametro de la funcion
    push buffer
    call writeCircularBuffer    ;escribo en el buffer circular    
    cmp al, 2
    jne limpiarStack            ;Se consulta si se detecto '\r'
    mov byte[flag_enter], 1     ;flag_enter=1 si hay '\r'. De lo contrario se procede a limpiar la pila.
limpiarStack:                   ;Se limpia la pila
    add esp, 4
    pop eax
    
    cmp al, 0x16        ;Consuta "U"=#UD
    je  exc_UD
    
    cmp al,0x17         ;Consulta "I"=#DF
    je  exc_DF

    cmp al,0x1F         ;#SS="S"
    je  exc_SS
    
    cmp al,0x1E         ;#AC="A"
    jne fin_int33

exc_AC: ;Genero expeción #AC    NO REALIZADO
    jmp fin_int33

;Genero expeción #UD. Códido inválido
exc_UD: 
    db 0xFF, 0xFF       ;Agrego un Códido inexistente causando Excepción 6
    jmp fin_int33
    
;Genero exceción #DF
exc_DF: 
    mov ax, 10
    mov bx, 0
    div bx
    jmp fin_int33
    
;Genero excepción #SS
exc_SS:
    LGDT [imagen_GDT]   ;Cargo la tabla GDT (Actualización) con un segmento no presente para el stack
    mov eax, ds_stack
    mov ss, eax
    
fin_int33:
    
    POPAD               ;Restauro registros de uso general.
    IRET                ;Fin de la interrupción.
    
;======================================================================;
; Manejador de excepción 6 #UD                                         ;
;======================================================================;
ManejadorExc6:
    PUSHAD              ;Salvo los registros de uso general.
        
    mov dl, 0x06        ;Indico en el registro DL la expeción 6
fin_int6:
    MOV AL,0x20         ;Envío End of Interrupt al PIC.
    OUT 0x20,AL

    POPAD               ;Restauro registros de uso general.
    
    IRET                ;Fin de la interrupción.
    
;======================================================================;
; Manejador de excepción 7 #NM  (Device Not Available)                 ;
;======================================================================;
ManejadorExc7:
    PUSHAD              ;Salvo los registros de uso general.
    mov ebp, esp
    clts                ;CR0.TS=0
    movzx eax, byte[tarea_actual]
    mov edx, 512
    mul edx
    FXRSTOR [contexto_simd+eax] ;Recupero contexto
    
fin_int7:
    mov esp, ebp
    MOV AL,0x20         ;Envío End of Interrupt al PIC.
    OUT 0x20,AL
    POPAD               ;Restauro registros de uso general.
    IRET                ;Fin de la interrupción.
    
;======================================================================;
; Manejador de excepción 8 #DF  (error code)                           ;
;======================================================================;
ManejadorExc8:
    PUSHAD              ;Salvo los registros de uso general.
    
    mov dl, 0x08        ;Indico en el registro DL la expeción 8
fin_int8:
    MOV AL,0x20         ;Envío End of Interrupt al PIC.
    OUT 0x20,AL
    
    POPAD               ;Restauro registros de uso general.
    add esp,4           ;elimino Error Code(0)
    IRET                ;Fin de la interrupción.
    
;======================================================================;
; Manejador de excepción 12 #SS (Error Code)                           ;
;======================================================================;
ManejadorExc12:
    PUSHAD              ;Salvo los registros de uso general.

    mov dl, 12          ;Indico en el registro DL la expeción 12
fin_int12:
    MOV AL,0x20         ;Envío End of Interrupt al PIC.
    OUT 0x20,AL

    POPAD               ;Restauro registros de uso general.
    add esp,4           ;elimino Error Code
    IRET                ;Fin de la interrupción.
    
;======================================================================;
; Manejador de excepción 14 #PF (Error Code)                           ;
;======================================================================;
ManejadorExc14:
    PUSHAD              ;Salvo los registros de uso general.

    mov ebp, esp
    mov eax, dword[ebp+8*4]             ;Tomo el Codigo de error
    
    ;Consulto si la expecion es dado por la Tarea 2 o 3, termino la excepcion.
    cmp byte[tarea_actual], 2
    je fin_int14
    cmp byte[tarea_actual], 3
    je fin_int14
    
    mov dword[string_k], "PAGE"
    mov dword[string_k+4], " FAU"
    mov dword[string_k+8], "LT: "
;    sub esp, 36
;    lea ebx, [ebp-36]
;    mov dword[ebx], "PAGE"
;    mov dword[ebx+4]," FAU"
;    mov dword[ebx+8], "LT: "
    
    mov edi, 12
Test_Bit_P:                 ;Bit Present
    bt  eax, 0              ;Realiza suma y modifica el Flag Carry
    jc Presente_PF          ;FC=1 => Página presente, FC=0 => Página no presente.
    
    call Asignar_pagina
    
    mov word[string_k+edi], "NP"
    add edi, 2
    jmp Test_Bit_WR
Presente_PF:
    mov byte[string_k+edi], "P"
    add edi, 1
    
Test_Bit_WR:            ;Bit Write(1)/Read(0).
    bt  eax, 1
    jc Write_PF
    mov word[string_k+edi], "-R"
    jmp Test_Bit_US
Write_PF:
    mov word[string_k+edi], "-W"
    
Test_Bit_US:            ;Bit User(1)/Supervisor(0)
    add edi, 2
    bt  eax, 2
    jc User_PF
    mov word[string_k+edi], "-S"
    jmp Test_Bit_RSBV
User_PF:
    mov word[string_k+edi], "-U"
    
Test_Bit_RSBV:          ;Bit Instruction fetch
    add edi, 2
    bt  eax, 3
    jnc Test_Bit_PK
    mov dword[string_k+edi], "-RS"
    add edi, 3
    
Test_Bit_PK:            ;Bit Protection keys
    bt  eax, 3
    jnc Test_Bit_SS
    mov dword[string_k+edi], "-PK"
    add edi, 3
    
Test_Bit_SS:            ;Bit Shadow-sack access
    bt  eax, 5
    jnc Test_Bit_SGX
    mov dword[string_k+edi], "-SS"
    add edi, 3
Test_Bit_SGX:           ;SGX
    bt  eax, 6
    jnc imprimirError
    mov dword[string_k+edi], "-SGX"
    add edi, 4
imprimirError:
    mov byte[string_k+edi], 0    ;agrego '\0'

    ;ImprimirKernel(int8 x, int8 y, char *str, int atributos)
    push 0x0F   ;atributos
    push string_k    ;*str
    push 0      ;y
    push 0      ;x
    call ImprimirKernel
    add esp, 4*4
    
fin_int14:
    mov esp, ebp
    MOV AL,0x20         ;Envío End of Interrupt al PIC.
    OUT 0x20,AL

    POPAD               ;Restauro registros de uso general.
    add esp,4           ;elimino Error Code
    IRET                ;Fin de la interrupción.

;======================================================================;
; Manejador de excepción 17 #AC (Error Code)                           ;
;======================================================================;
ManejadorExc17:
    PUSHAD              ;Salvo los registros de uso general.
    
    mov dl, 17         ;Indico en el registro DL la expeción 17
fin_int17:
    MOV AL,0x20         ;Envío End of Interrupt al PIC.
    OUT 0x20,AL

    POPAD               ;Restauro registros de uso general.
    add esp,4           ;elimino Error Code
    IRET                ;Fin de la interrupción.
