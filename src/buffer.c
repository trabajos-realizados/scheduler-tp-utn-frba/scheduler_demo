#define TAIL        4
#define HEAD        0
#define LARGO       (17+2)
#define MAX_DATOS   17
#define SIZE_BUF    (LARGO+2*4)

typedef unsigned char       uint8_t;
typedef unsigned short int  uint16_t;
typedef unsigned int        uint32_t;
typedef unsigned long long  uint64_t;

typedef struct
{
    char *headPointer;      //Puntero de lectura
    char *tailPointer;      //Puntero de escritura
    char buf[MAX_DATOS];    //Buffer de datos
    uint8_t cant_datos;     //Cantidad de datos en el buffer
}CircularBuffer;

/* Inicializa el buffer circular.
 * param *circular_buffer:  puntero del buffer circular
 */
void initCircularBuffer(CircularBuffer *circular_buffer)
{    
    circular_buffer->cant_datos = 0;    //Se indica que no hay datos
    circular_buffer->headPointer = circular_buffer->buf;    //Ambos punteros apuntan a la misma direccion dado que esta vacio.
    circular_buffer->tailPointer = circular_buffer->buf;
}

/* Lee un caracter del buffer.
 * param *circular_buffer: puntero del buffer circular.
 * return char: Retorna el caracter leido, en caso de error retorna 0
 *
 */
char readCircularBuffer(CircularBuffer *circular_buffer)
{
    char caracter;
    
    if( circular_buffer->cant_datos != 0 )  //Verificar que hayan datos en el buffer
    {
        caracter = *(circular_buffer->headPointer); //Leo celda
        *(circular_buffer->headPointer) = 0;     //Limpio celda de memoria
        
        if( circular_buffer->headPointer == &(circular_buffer->buf[MAX_DATOS-1]) )  //Verfico si el puntero esta en el extremo del buffer
        {
            circular_buffer->headPointer = circular_buffer->buf;                    //Si lo esta, reapunta al inicio.
        }
        else
        {
            circular_buffer->headPointer = circular_buffer->headPointer + 1;        //De lo contrario, a la siguiente direccion
        }
        
        circular_buffer->cant_datos--;  //Decremento la cantidad de datos presentes en el buffer
    }
    else
        caracter = 0;
        
    return caracter;
}

/* Escribe un caracter del buffer.
 * param *circular_buffer: puntero del buffer circular.
 * param dato:  informacion a escribir en el buffer.
 * return uint8_t: Retorna el 1 en caso de exito, ante un error 0. Cuando se detecta '\r' devuelve 2.
 */
uint8_t writeCircularBuffer(CircularBuffer *circular_buffer, char dato)
{
    uint8_t retorno;
    
    if( (dato<2 || dato>11) && dato!=0x1C)   //Si no es un numero entre el 0 y el 9 o es '\r',
        return 0;
    
    if( circular_buffer->cant_datos == (MAX_DATOS-1) )   //Si se alcanza el maximo de datos a guardar reemplazar por '\r'
    {
        dato = 0x1C;        //En el caracter num 17 se reemplaza por '\r'
    }
    
    if( circular_buffer->cant_datos < MAX_DATOS )   //Verificar si hay espacio disponible
    {
        *(circular_buffer->tailPointer) = dato;     //Se guarda el dato        
        
        if( circular_buffer->tailPointer == &(circular_buffer->buf[MAX_DATOS-1]) )  //Verfico si el puntero esta en el extremo del buffer
        {
            circular_buffer->tailPointer = circular_buffer->buf;                    //Si lo esta, reapunta al inicio.
        }
        else
        {
            circular_buffer->tailPointer = circular_buffer->tailPointer + 1;        //De lo contrario, a la siguiente direccion
        }
        
        circular_buffer->cant_datos++;  //Decremento la cantidad de datos presentes en el buffer
        
        if(dato == 0x1C)
            retorno = 2;
        else
            retorno = 1;
    }
    else
        retorno = 0;
        
    return retorno;
}
