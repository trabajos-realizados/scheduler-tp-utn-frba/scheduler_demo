;*****************************************************************************;
;                               TAREA 3                                       ;
;*****************************************************************************;

;Funciones C

;Funicones ASM
extern sel_sysCall_imprimirPantalla
extern sel_sysCall_cantidadTablaDigitos
extern sel_sysCall_readTablaDigitos
extern sel_sysCall_read_dir
extern sel_sysCall_halt

;Definiciones
SIZE_STACK_TAREA_3  equ 4*1024     ;Tamano en bytes

;*****************************************************************************;
section .bss_tarea_3 NOBITS
suma        resb    8       ;Int de 64 bits
string      resb    16+1    ;String para guardar un numero de 64 bits

;*****************************************************************************;
section .data_tarea_3 PROGBITS

;*****************************************************************************;
section .rodata_tarea_3 PROGBITS
mensaje     db  'Tarea 3:', 0

;*****************************************************************************;
section .stack_tarea_3 NOBITS
stack_tarea_3   resb    SIZE_STACK_TAREA_3

;*****************************************************************************;
section .text_tarea_3 PROGBITS
Tarea_3:
    ;SE IMPRIME EL MENSAJE INDICANDO LA TAREA
    ;ImprimirPantalla(int8 x, int8 y, char *str, int atributos);
    ;Atributos: 1B [blink, R, G, B(fondo), brillo, R, G, B(frente)]       ;
    push 0x0F           ;atributos
    push mensaje        ;*str
    push 3              ;y->3
    push 54             ;x->54
    call sel_sysCall_imprimirPantalla:0
    add esp, 4*4
    
    ;uint16_t sel_sysCall_cantidadTablaDigitos(). AX=cantidad de datos.
    call sel_sysCall_cantidadTablaDigitos:0 ;Verifico que hayan datos en la tabla de datos.
    cmp ax, 0
    je fin_tarea_3
    
    movzx ecx, ax   ;ECX=cantidad de datos en la tabla.
    mov edi, 0      ;Inicializo el indice auxiliar.
    pxor mm0, mm0   ;mm0=0 inicializo mm0
    pxor mm2, mm2   ;mm2=0 inicializo mm2

sumatoria:    
    ;uint64_t readTablaDigitos(uint32_t indice). Leo un valor de la tabla indicado por el indice.
    push edi                                ;Indice
    call sel_sysCall_readTablaDigitos:0     ;Valor en EDX:EAX
    add esp, 4                              ;Balance de pila
    
    movd mm1, edx               ;Guardo en mm1 el DWORD ALTO.
    psllq mm1, 32               ;Desplazo 32 bit el empaquetado de 64bits.
    movd mm2, eax               ;En mm2 guado el DWORD BAJO del numero obtenido de la tabla.
    paddq mm1, mm2              ;Le sumo a mm1 el DWORD BAJO, conformando el numero de 64 bits.
    paddq mm0, mm1              ;Suma en tamaño quadword.
    inc edi                     ;EDI++
    cmp edi, ecx                ;Comparo EDI con ECX:cantidad de datos en la tabla.
    jne sumatoria               ;Si es diferente, entonces faltan numeros por sumar, salto a sumatoria.
    movq [suma], mm0            ;Caso contrario guardo el resultado.
    
    ;void itoa64(uint64_t num, char *str)
    push string         ;*string
    push dword[suma+4]  ;num
    push dword[suma]    ;num
    call itoa64
    add esp,4*3         ;Equilibro pila
    
    ;ImprimirPantalla(int8 x, int8 y, char *str, int atributos);
    ;Atributos: 1B [blink, R, G, B(fondo), brillo, R, G, B(frente)]       ;
    push 0x0F           ;atributos
    push string         ;*str
    push 3              ;y->3
    push 64             ;x->64
    call sel_sysCall_imprimirPantalla:0
    add esp, 4*4
    
fin_tarea_3:
    call sel_sysCall_halt:0
    jmp Tarea_3
;=============================================================================;
;Sección Include.
include_tarea_3:
    %include "./inc/stdio.inc"
