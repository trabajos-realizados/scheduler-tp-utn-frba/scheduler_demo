global imagen_GDT
global cs_sel
global cs_sel_pl3
global ds_sel
global ds_sel_pl3
global ds_stack
global sel_TSS_inicial
global sel_sysCall_halt
global sel_sysCall_read
global sel_sysCall_print
global sel_sysCall_cantidadTablaDigitos
global sel_sysCall_readTablaDigitos
global sel_sysCall_imprimirPantalla
global sel_sysCall_read_dir
global IDT_exc6
global IDT_exc7
global IDT_exc8
global IDT_exc12
global IDT_exc14
global IDT_exc17
global IDT_teclado
global IDT_timer
global IDTR
;*****************************************************************************;
section .tables

;TABLA DE DESCRIPTORES GDT. 
GDT:    dq  0       ;Primeros 8B van vacios
cs_sel  equ $-GDT
;Descriptores. Limite = 0xFFFFFFFF, Base = 0x00000000
    dw  0xFFFF      ;Limite 15-0
    dw  0           ;Base 15-0
    db  0           ;Base 23-16
    db  0x9A        ;Derechos de acceso: P=1; DPL=0b00; b4=1; b3=1(codigo); b2=0 (no conforme); R=1; b0=0.
    db  0xCF        ;G=1; D=1 (32-bits); limite 19-16
    db  0           ;Base 31-24
cs_sel_pl3  equ $-GDT+3 ;Descriptor de datos. RPL=3. Descriptores. Limite = 0xFFFFFFFF, Base = 0x00000000
    dw  0xFFFF      ;Limite 15-0
    dw  0           ;Base 15-0
    db  0           ;Base 23-16
    db  0xFA        ;Derechos de acceso. CPL=3. Descriptor de código.
    db  0xCF        ;G=1 D=1 limite 19-16
    db  0           ;Base 31-24
ds_sel  equ $-GDT   ;Descriptor de datos. limite 4GB y base 0
    dw  0xFFFF      ;Limite 15-0
    dw  0x0000      ;Base 15-0
    db  0x00        ;Base 23-16
    db  0x92        ;Derechos de acceso: P=1; DPL=0b00; b4=1; b3=0(datos); b2=0 (expansion); W=1; b0=0.
    db  0xCF        ;G=1; D=1 (32-bits); limite 19-16
    db  0x00        ;Base 31-24
ds_sel_pl3  equ $-GDT+3   ;Descriptor de datos. RPL=3
    dw  0xFFFF      ;Limite 15-0
    dw  0x0000      ;Base 15-0
    db  0x00        ;Base 23-16
    db  0xF2        ;Derechos de acceso. DPL=3
    db  0xCF        ;G=1, D=1, limite 19-16
    db  0x00        ;Base 31-24
ds_stack  equ $-GDT ;Descriptor de datos. Limite = 0x000FFFFF, Base = 0x1FFF0000
    dw  0xFFFF      ;Limite 15-0
    dw  0x0         ;Base 15-0
    db  0xFF        ;Base 23-16
    db  0x12        ;Derechos de acceso
    db  0x40        ;G=0, D=1, limite 19-16
    db  0x1F        ;Base 31-24
sel_TSS_inicial equ $-GDT    ;DESCRIPTOR DE TSS
    dw 0x68-1       ;límite TSS 15-0
    dw 0            ;base TSS 15-0
    db 0            ;base TSS 23-16
    db 0x89         ;Derechos de acceso: Descriptor TSS; DPL=0b00; Busy=0;
    db 0x00         ;límite TSS 19-16, flags
    db 0            ;base TSS 31-24
    
    times (0x80-7) dq 0   ;Compuertas no usadas hasta  0x79
    
; Compuertas de llamada a partir de 0x80.
;----------------------------------------

GDT_td3_halt:
sel_sysCall_halt equ $-GDT  ;Selector RPL=0. Compuerta de llamada: void td3_halt();
    dw 0             ;Offset 15-0
    dw cs_sel        ;Selector de código pl=0.
    db 0             ;0 parámetros.
    db 0xEC          ;Compuerta de llamada. Nivel de privilegio 3
    dw 0             ;Offset 31-16
    
GDT_td3_read:
sel_sysCall_read equ $-GDT  ;RPL=0. Compuerta de llamada: unsigned int td3_read(void *buffer, unsigned int num_bytes);
    dw 0             ;Offset 15-0
    dw cs_sel        ;Selector de código pl=0.
    db 0             ;0 parámetros.
    db 0xEC          ;Compuerta de llamada. Nivel de privilegio 3
    dw 0             ;Offset 31-16
    
GDT_td3_print:
sel_sysCall_print equ $-GDT ;RPL=0. Compuerta de llamada: unsigned int td3_print(void *buffer, unsigned int num_bytes);
    dw 0             ;Offset 15-0
    dw cs_sel        ;Selector de código pl=0.
    db 0             ;0 parámetros.
    db 0xEC          ;Compuerta de llamada. Nivel de privilegio 3
    dw 0             ;Offset 31-16
    
GDT_cantidadTablaDigitos:
sel_sysCall_cantidadTablaDigitos equ $-GDT ;RPL=0. Compuerta de llamada: uint16_t cantidadTablaDigitos();
    dw 0             ;Offset 15-0
    dw cs_sel        ;Selector de código pl=0.
    db 0             ;0 parámetros.
    db 0xEC          ;Compuerta de llamada. Nivel de privilegio 3
    dw 0             ;Offset 31-16
    
sel_sysCall_readTablaDigitos equ $-GDT ;RPL=0. Compuerta de llamada: uint64_t readTablaDigitos(uint32_t);
    dw 0             ;Offset 15-0
    dw cs_sel        ;Selector de código pl=0.
    db 0             ;0 parámetros.
    db 0xEC          ;Compuerta de llamada. Nivel de privilegio 3
    dw 0             ;Offset 31-16

sel_sysCall_imprimirPantalla equ $-GDT ;RPL=0. Compuerta de llamada.
    dw 0             ;Offset 15-0
    dw cs_sel        ;Selector de código pl=0.
    db 0             ;0 parámetros.
    db 0xEC          ;Compuerta de llamada. Nivel de privilegio 3
    dw 0             ;Offset 31-16
    
sel_sysCall_read_dir equ $-GDT ;RPL=0. Compuerta de llamada.
    dw 0             ;Offset 15-0
    dw cs_sel        ;Selector de código pl=0.
    db 0             ;0 parámetros.
    db 0xEC          ;Compuerta de llamada. Nivel de privilegio 3
    dw 0             ;Offset 31-16
long_GDT    equ $-GDT

imagen_GDT:
    dw  long_GDT-1          ;Limite
    dd  (GDT)  ;Base

;TABLA IDT.   
IDT:
    TIMES 6 DQ 0        ;Compuertas no usadas para int 0 a 5.

;Compuerta de expeción 6 #UD
IDT_exc6:
    dw 0        ;Offset 15-0
    dw cs_sel   ;Selector 15-0
    db 0
    db 0x8F     ;Compuerta de excepción
    dw 0x00     ;Offset 31-36

;Compuerta de expeción 7 #NM
IDT_exc7:
    dw 0        ;Offset 15-0
    dw cs_sel   ;Selector 15-0
    db 0
    db 0x8F     ;Compuerta de excepción
    dw 0x00     ;Offset 31-36

;Compuerta de excepción 8 #DF
IDT_exc8:
    dw 0        ;Offset 15-0
    dw cs_sel   ;Selector 15-0
    db 0
    db 0x8F     ;Compuerta de excepción
    dw 0x00     ;Offset 31-36
    
    times (11-8) dq 0   ;Compuertas no usadas de int 9 a 11

;Compuerta de expeción 12 #SS
IDT_exc12:
    dw 0        ;Offset 15-0
    dw cs_sel   ;Selector 15-0
    db 0
    db 0x8F     ;Compuerta de excepción
    dw 0x00     ;Offset 31-36
    
;Compuerta no usada int 13
    dq 0   
    
;Compuerta de excepción 14 #PF
IDT_exc14:
    dw 0        ;Offset 15-0
    dw cs_sel   ;Selector 15-0
    db 0
    db 0x8F     ;Compuerta de excepción
    dw 0x00     ;Offset 31-24

;Compuertas no usadas de int 15 a 16
    times (16-14) dq 0
    
;Compuerta de excepción 17 #AC
IDT_exc17:
    dw 0        ;Offset 15-0
    dw cs_sel   ;Selector 15-0
    db 0
    db 0x8F     ;Compuerta de excepción
    dw 0x00     ;Offset 31-36

    times (31-17)    dq 0        ;Compuertas no usadas para int 17 al 31
    
; Compuerta de interrupción del timer 0x20 (32).
IDT_timer:
    dw 0                   ;Offset 15-0
    dw cs_sel              ;Selector.
    db 0                   ;No usado.
    db 0x8F                ;Compuerta de interrupción. Nivel de privilegio 0
    dw 0
    
; Compuerta de interrupción de teclado 0x21 (33).
IDT_teclado:
    dw 0                   ;Offset 15-0
    dw cs_sel              ;Selector.
    db 0                   ;No usado.
    db 0x8F                ;Compuerta de interrupción. Nivel de privilegio 0
    dw 0                   ;Offset 31-16
IDT_size  EQU $-IDT        ;Tamaño de la IDT.

IDTR    DW IDT_size-1      ;Limite
        DD IDT             ;Base
