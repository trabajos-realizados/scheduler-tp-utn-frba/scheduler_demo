;Direcciones
extern __init_ROM
extern __page_tables
extern __page_tables_DF
extern __video_DF

extern __datos_DF
extern __size_datos

extern __tabla_digitos_DF
extern __bss_tarea_1_DF

extern __stack_kernel_DF
extern __init_stack_kernel
extern __size_stack_kernel

extern __kernel
extern __kernel_DF
extern __init_kernel_LMA
extern __size_kernel

extern __rutinas
extern __rutinas_DF
extern __init_rutinas_LMA
extern __size_rutinas

extern __sys_tables
extern __sys_tables_DF
extern __init_sys_tables_LMA
extern __size_sys_tables

extern __text_tarea_1
extern __text_tarea_1_DF
extern __init_text_tarea_1_LMA
extern __size_text_tarea_1

extern __data_tarea_1    
extern __data_tarea_1_DF
extern __init_data_tarea_1_LMA
extern __size_data_tarea_1

extern __rodata_tarea_1
extern __rodata_tarea_1_DF
extern __init_rodata_tarea_1_LMA
extern __size_rodata_tarea_1

extern __stack_tarea_1_DF
extern __init_stack_tarea_1
extern __size_stack_tarea_1

extern __init_stack_kernel_tarea_1
extern __size_stack_kernel_tarea_1
extern __stack_kernel_tarea_1_DF

extern __bss_tarea_1_DF
extern __size_bss_tarea_1

extern __text_tarea_2
extern __text_tarea_2_DF
extern __init_text_tarea_2_LMA
extern __size_text_tarea_2

extern __data_tarea_2    
extern __data_tarea_2_DF
extern __init_data_tarea_2_LMA
extern __size_data_tarea_2

extern __rodata_tarea_2
extern __rodata_tarea_2_DF
extern __init_rodata_tarea_2_LMA
extern __size_rodata_tarea_2

extern __stack_tarea_2_DF
extern __size_stack_tarea_2
extern __init_stack_tarea_2

extern __bss_tarea_2_DF
extern __size_bss_tarea_2

extern __init_stack_kernel_tarea_2
extern __size_stack_kernel_tarea_2
extern __stack_kernel_tarea_2_DF

extern __text_tarea_3
extern __text_tarea_3_DF
extern __init_text_tarea_3_LMA
extern __size_text_tarea_3

extern __data_tarea_3
extern __data_tarea_3_DF
extern __init_data_tarea_3_LMA
extern __size_data_tarea_3

extern __rodata_tarea_3
extern __rodata_tarea_3_DF
extern __init_rodata_tarea_3_LMA
extern __size_rodata_tarea_3

extern __stack_tarea_3_DF
extern __init_stack_tarea_3
extern __size_stack_tarea_3

extern __bss_tarea_3_DF
extern __size_bss_tarea_3

extern __init_stack_kernel_tarea_3
extern __size_stack_kernel_tarea_3
extern __stack_kernel_tarea_3_DF

extern __text_tarea_4
extern __text_tarea_4_DF
extern __init_text_tarea_4_LMA
extern __size_text_tarea_4

extern __data_tarea_4    
extern __data_tarea_4_DF
extern __init_data_tarea_4_LMA
extern __size_data_tarea_4

extern __rodata_tarea_4
extern __rodata_tarea_4_DF
extern __init_rodata_tarea_4_LMA
extern __size_rodata_tarea_4

extern __stack_tarea_4_DF
;extern __init_stack_tarea_4
;extern __size_stack_tarea_4

extern __bss_tarea_4_DF
extern __size_bss_tarea_4

extern __init_stack_kernel_tarea_4
extern __size_stack_kernel_tarea_4
extern __stack_kernel_tarea_4_DF

;Etiquetas ASM
extern cs_sel
extern cs_sel_pl3
extern ds_sel
extern ds_sel_pl3
extern sel_sysCall_halt
extern sel_sysCall_read
extern sel_sysCall_print
extern sel_sysCall_cantidadTablaDigitos
extern sel_sysCall_readTablaDigitos
extern sel_sysCall_imprimirPantalla
extern sel_sysCall_read_dir
extern imagen_GDT
extern sel_TSS_inicial
extern IDT_exc6
extern IDT_exc7
extern IDT_exc8
extern IDT_exc12
extern IDT_exc14
extern IDT_exc17
extern IDT_teclado
extern IDT_timer
extern IDTR

;Funciones ASM
extern ManejadorExc6
extern ManejadorExc7
extern ManejadorExc8
extern ManejadorExc12
extern ManejadorExc14
extern ManejadorExc17
extern ManejadorTeclado
extern ManejadorTimer
extern td3_halt
extern td3_read
extern td3_print
extern cantidadTablaDigitos
extern readTablaDigitos
extern ImprimirPantalla
extern read_Dir

;Variables
extern TSS_inicial
extern contexto_pila

;Definiciones
SIZE_STACK_KERNEL           equ 27*1024     ;pila en B
SIZE_STACK_KERNEL_TAREA_1   equ 1000
SIZE_STACK_KERNEL_TAREA_2   equ 1*1024
SIZE_STACK_KERNEL_TAREA_3   equ 1*1024
SIZE_STACK_KERNEL_TAREA_4   equ 1*1024

;*****************************************************************************;
;   INCLUDES                                                                  ;
;*****************************************************************************;
    %include "./inc/paginacion.inc"

;*****************************************************************************;
BITS 32

section .stack_kernel  NOBITS
pila    resb    SIZE_STACK_KERNEL  ;Reservo espacio para el stack

;*****************************************************************************;
section .stack_kernel_tarea_1  NOBITS
pila_kernel_T1  resb    SIZE_STACK_KERNEL_TAREA_1   ;Reservo espacio para el stack

;*****************************************************************************;
section .stack_kernel_tarea_2  NOBITS
pila_kernel_T2  resb    SIZE_STACK_KERNEL_TAREA_2   ;Reservo espacio para el stack

;*****************************************************************************;
section .stack_kernel_tarea_3  NOBITS
pila_kernel_T3  resb    SIZE_STACK_KERNEL_TAREA_3   ;Reservo espacio para el stack

;*****************************************************************************;
section .stack_kernel_tarea_4  NOBITS
pila_kernel_T4  resb    SIZE_STACK_KERNEL_TAREA_4   ;Reservo espacio para el stack

;*****************************************************************************;
section .tabla_digitos    NOBITS
tabla resb 8*4

;*****************************************************************************;
    section .text
Inicio:
    incbin "./bin/init16.bin"

    mov esp, __init_stack_kernel    ;Inicializo pila, debe ser multiplito de 4B
    mov ebp, esp
    
; Inicializar controlador de teclado.
    MOV AL,0xFF         ;Enviar comando de reset al controlador
    OUT 0x64,AL         ;de teclado
    MOV ECX,256         ;Esperar que rearranque el controlador.
    LOOP $
    MOV ECX,0x10000
ciclo1:
    IN AL,0x60          ;Esperar que termine el reset del controlador.
    TEST AL,1
    LOOPZ ciclo1
    MOV AL,0xF4         ;Habilitar el teclado.
    OUT 0x64,AL
    MOV ECX,0x10000
ciclo2:
    IN AL,0x60          ;Esperar que termine el comando.
    TEST AL,1
    LOOPZ ciclo2
    IN AL,0x60          ;Vaciar el buffer de teclado.
;Inicializar timer para que interrumpa cada 50 milisegundos.
    MOV AL,00110100b    ;Canal cero, byte bajo y luego byte alto.
    OUT 0x43,AL
    MOV AL,0x64         ;F=1193181Hz/(2^16-AX) => AX=2^16-1193181Hz*T => AX=53604=0xD164
    OUT 0x40,AL         ;Programar byte bajo del timer de 16 bits.
    MOV AL, 0xD1
    OUT 0x40,AL         ;Programar byte alto del timer de 16 bits.
; Inicializar ambos PIC usando ICW (Initialization Control Words).
; ICW1 = Indicarle a los PIC que estamos inicializándolo.
    MOV AL,0x11         ;Palabra de inicialización (bit 4=1) indicando que 
                        ;se necesita ICW4 (bit 0=1)
    OUT 0x20,AL         ;Enviar ICW1 al primer PIC.
    OUT 0xA0,AL         ;Enviar ICW1 al segundo PIC.
        
; ICW2 = Indicarle a los PIC cuales son los vectores de interrupciones.
    MOV AL,0x20         ;El primer PIC va a usar los tipos de interr 0x20-0x27.
    OUT 0x21,AL         ;Enviar ICW2 al primer PIC.
    MOV AL,0x28         ;El segundo PIC va a usar los tipos de interr 0x28-0x2F.
    OUT 0xA1,AL         ;Enviar ICW2 al segundo PIC.
        
; ICW3 = Indicarle a los PIC como se conectan como master y slave.        
    MOV AL,4            ;Decirle al primer PIC que hay un PIC esclavo en IRQ2.
    OUT 0x21,AL         ;Enviar ICW3 al primer PIC.
    MOV AL,2            ;Decirle al segundo PIC su ID de cascada (2).
    OUT 0xA1,AL         ;Enviar ICW3 al segundo PIC.
        
; ICW4 = Información adicional sobre el entorno.    
    MOV AL,1            ;Poner el PIC en modo 8086.
    OUT 0x21,AL         ;Enviar ICW4 al primer PIC.
    OUT 0xA1,AL         ;Enviar ICW4 al segundo PIC.

; Indicar cuales son los IRQ habilitados.
    MOV AL,11111100b    ;Activar solo IRQ0 (timer) e IRQ1 (teclado) poniendo estos bits a cero.
    OUT 0x21,AL         ;Enviar máscara al primer PIC.

;Configuracion de Paginacion
    cld                             ;Direcciones crecientes
    mov edi, __page_tables          ;Apuntar al inicio de la 1ra tabla.
    mov ecx, SIZE_ENTRY_PAGE        ;Cantidad de entradas del directorio y tabla.
    xor eax,eax                     ;Se limpia las entradas de paginas.
    rep stosd
    
;****************************************************************
;Paginacion Tarea 1
    mov esi, DIR_PAG_1
    call kernel_paginacion          ;ESI: directorio de paginas
    
;Directorios de paginas
    mov dword[DIR_PAG_1+4*0x0BF], TABLA_PAG_1a+USR+WE+P  ;Pila tarea 1
    mov dword[DIR_PAG_1+4*0x004], TABLA_PAG_1a+USR+WE+P  ;Text, bss, data, rodata
    mov dword[DIR_PAG_1+4*0x0FF], TABLA_PAG_1b+SUP+WE+P  ;Pila kernel tarea 1
    
;Tabla de paginas    
    ;Tarea 1 text
    mov eax, __size_text_tarea_1
    lea edi, [TABLA_PAG_1a+4*0x310]
    mov ebx, __text_tarea_1_DF+USR+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Tarea 1 bss
    mov eax, __size_bss_tarea_1
    lea edi, [TABLA_PAG_1a+4*0x320]
    mov ebx, __bss_tarea_1_DF+USR+WE+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes

    ;Tarea 1 data
    mov eax, __size_data_tarea_1
    lea edi, [TABLA_PAG_1a+4*0x330]
    mov ebx, __data_tarea_1_DF+USR+WE+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Tarea 1 rodata
    mov eax, __size_rodata_tarea_1
    lea edi, [TABLA_PAG_1a+4*0x340]
    mov ebx, __rodata_tarea_1_DF+USR+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes

    ;Tarea 1 Stack
    mov eax, __size_stack_tarea_1+1
    lea edi, [TABLA_PAG_1a+4*0x3FF]
    mov ebx, __stack_tarea_1_DF+USR+WE+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Tarea 1 Stack kernel
    mov eax, __size_stack_kernel_tarea_1+1
    lea edi, [TABLA_PAG_1b+4*0x3FF]
    mov ebx, __stack_kernel_tarea_1_DF+SUP+WE+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
;****************************************************************
;Paginacion Tarea 2
    mov esi, DIR_PAG_2
    call kernel_paginacion          ;ESI: directorio de paginas
    
;Directorios de paginas
    mov dword[DIR_PAG_2+4*0x0C0], TABLA_PAG_2b+USR+WE+P  ;Pila tarea 2
    mov dword[DIR_PAG_2+4*0x005], TABLA_PAG_2b+USR+WE+P  ;Text, bss, data, rodata
    mov dword[DIR_PAG_2+4*0x100], TABLA_PAG_2c+SUP+WE+P  ;Pila kernel tarea 2
    
;Tabla de paginas    
    ;Tarea 2 text
    mov eax, __size_text_tarea_2
    lea edi, [TABLA_PAG_2b+4*0x010]
    mov ebx, __text_tarea_2_DF+USR+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Tarea 2 bss
    mov eax, __size_bss_tarea_2
    lea edi, [TABLA_PAG_2b+4*0x020]
    mov ebx, __bss_tarea_2_DF+USR+WE+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Tarea 2 data
    mov eax, __size_data_tarea_2
    lea edi, [TABLA_PAG_2b+4*0x030]
    mov ebx, __data_tarea_2_DF+USR+WE+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Tarea 2 rodata
    mov eax, __size_rodata_tarea_2
    lea edi, [TABLA_PAG_2b+4*0x040]
    mov ebx, __rodata_tarea_2_DF+USR+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Tarea 2 Stack
    mov eax, __size_stack_tarea_2+1
    lea edi, [TABLA_PAG_2b+4*0x000]
    mov ebx, __stack_tarea_2_DF+USR+WE+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Tarea 2 Stack kernel
    mov eax, __size_stack_kernel_tarea_2+1
    lea edi, [TABLA_PAG_2c+4*0x000]
    mov ebx, __stack_kernel_tarea_2_DF+SUP+WE+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
;****************************************************************
;Paginacion Tarea 3
    mov esi, DIR_PAG_3
    call kernel_paginacion          ;ESI: directorio de paginas
    
;Directorios de paginas
    mov dword[DIR_PAG_3+4*0x0C0], TABLA_PAG_3b+USR+WE+P  ;Pila tarea 3
    mov dword[DIR_PAG_3+4*0x005], TABLA_PAG_3b+USR+WE+P  ;Text, bss, data, rodata
    mov dword[DIR_PAG_3+4*0x100], TABLA_PAG_3c+SUP+WE+P  ;Pila kernel tarea 3
    
;Tabla de paginas    
    ;Tarea 3 text
    mov eax, __size_text_tarea_3
    lea edi, [TABLA_PAG_3b+4*0x110]
    mov ebx, __text_tarea_3_DF+USR+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Tarea 3 bss
    mov eax, __size_bss_tarea_3
    lea edi, [TABLA_PAG_3b+4*0x120]
    mov ebx, __bss_tarea_3_DF+USR+WE+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Tarea 3 data
    mov eax, __size_data_tarea_3
    lea edi, [TABLA_PAG_3b+4*0x130]
    mov ebx, __data_tarea_3_DF+USR+WE+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Tarea 3 rodata
    mov eax, __size_rodata_tarea_3
    lea edi, [TABLA_PAG_3b+4*0x140]
    mov ebx, __rodata_tarea_3_DF+USR+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Tarea 3 Stack
    mov eax, __size_stack_tarea_3+1
    lea edi, [TABLA_PAG_3b+4*0x001]
    mov ebx, __stack_tarea_3_DF+USR+WE+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Tarea 3 Stack kernel
    mov eax, __size_stack_kernel_tarea_3+1
    lea edi, [TABLA_PAG_3c+4*0x001]
    mov ebx, __stack_kernel_tarea_3_DF+SUP+WE+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
;****************************************************************
;Paginacion Tarea 4
    mov esi, DIR_PAG_4
    call kernel_paginacion          ;ESI: directorio de paginas
    
;Directorios de paginas
    ;mov dword[DIR_PAG_4+4*0x0C0], TABLA_PAG_4b+SUp+WE+P  ;Pila tarea 4
    mov dword[DIR_PAG_4+4*0x005], TABLA_PAG_4b+SUP+WE+P  ;Text, bss, data, rodata
    mov dword[DIR_PAG_4+4*0x100], TABLA_PAG_4c+SUP+WE+P  ;Pila kernel tarea 4
    
;Tabla de paginas    
    ;Tarea 4 text
    mov eax, __size_text_tarea_4
    lea edi, [TABLA_PAG_4b+4*0x210]
    mov ebx, __text_tarea_4_DF+SUP+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Tarea 4 bss
    mov eax, __size_bss_tarea_4
    lea edi, [TABLA_PAG_4b+4*0x220]
    mov ebx, __bss_tarea_4_DF+SUP+WE+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Tarea 4 data
    mov eax, __size_data_tarea_4
    lea edi, [TABLA_PAG_4b+4*0x230]
    mov ebx, __data_tarea_4_DF+SUP+WE+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Tarea 4 rodata
    mov eax, __size_rodata_tarea_4
    lea edi, [TABLA_PAG_4b+4*0x240]
    mov ebx, __rodata_tarea_4_DF+SUP+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Tarea 4 Stack
    ;mov eax, __size_stack_tarea_4+1
    ;lea edi, [TABLA_PAG_4b+4*0x002]
    ;mov ebx, __stack_tarea_4_DF+SUP+WE+P
    ;call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Tarea 4 Stack kernel
    mov eax, __size_stack_kernel_tarea_4+1
    lea edi, [TABLA_PAG_4c+4*0x002]
    mov ebx, __stack_kernel_tarea_4_DF+SUP+WE+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes

;Cargo parámetros de td3_memcopy(destino, origen, num_bytes);

;Copio el codigo principal
    push __size_kernel          ;Tamaño
    push __init_kernel_LMA      ;Origen
    push __kernel_DF            ;Destino
    call td3_memcopy            ;Llamo subrutina
    add esp, 4*3 
    
;Copio tablas de Descriptores
    push __size_sys_tables      ;Tamaño
    push __init_sys_tables_LMA  ;Origen
    push __sys_tables_DF        ;Destino
    call td3_memcopy
    add esp, 4*3 

;Copio rutinas
    push __size_rutinas         ;Tamaño
    push __init_rutinas_LMA     ;Origen
    push __rutinas_DF           ;Destino
    call td3_memcopy
    add esp, 4*3 
    
;Copio tarea 1 text
    push __size_text_tarea_1    ;Tamaño 
    push __init_text_tarea_1_LMA;Orgien
    push __text_tarea_1_DF      ;Destino
    call td3_memcopy
    add esp, 4*3 
    
;Copio tarea 1 data 
    push __size_data_tarea_1    ;Tamaño 
    push __init_data_tarea_1_LMA;Orgien
    push __data_tarea_1_DF      ;Destino
    call td3_memcopy
    add esp, 4*3 
    
;Copio tarea 1 rodata
    push __size_rodata_tarea_1    ;Tamaño 
    push __init_rodata_tarea_1_LMA;Orgien
    push __rodata_tarea_1_DF      ;Destino
    call td3_memcopy
    add esp, 4*3
    
;Copio tarea 2 text
    push __size_text_tarea_2    ;Tamaño 
    push __init_text_tarea_2_LMA;Orgien
    push __text_tarea_2_DF      ;Destino
    call td3_memcopy
    add esp, 4*3 
    
;Copio tarea 2 data 
    push __size_data_tarea_2    ;Tamaño 
    push __init_data_tarea_2_LMA;Orgien
    push __data_tarea_2_DF      ;Destino
    call td3_memcopy
    add esp, 4*3 
    
;Copio tarea 2 rodata
    push __size_rodata_tarea_2    ;Tamaño 
    push __init_rodata_tarea_2_LMA;Orgien
    push __rodata_tarea_2_DF      ;Destino
    call td3_memcopy
    add esp, 4*3
    
;Copio tarea 3 text
    push __size_text_tarea_3    ;Tamaño 
    push __init_text_tarea_3_LMA;Orgien
    push __text_tarea_3_DF      ;Destino
    call td3_memcopy
    add esp, 4*3 
    
;Copio tarea 3 data 
    push __size_data_tarea_3    ;Tamaño 
    push __init_data_tarea_3_LMA;Orgien
    push __data_tarea_3_DF      ;Destino
    call td3_memcopy
    add esp, 4*3 
    
;Copio tarea 3 rodata
    push __size_rodata_tarea_3    ;Tamaño 
    push __init_rodata_tarea_3_LMA;Orgien
    push __rodata_tarea_3_DF      ;Destino
    call td3_memcopy
    add esp, 4*3
    
;Copio tarea 4 text
    push __size_text_tarea_4    ;Tamaño 
    push __init_text_tarea_4_LMA;Orgien
    push __text_tarea_4_DF      ;Destino
    call td3_memcopy
    add esp, 4*3 
    
;Copio tarea 4 data 
    push __size_data_tarea_4    ;Tamaño 
    push __init_data_tarea_4_LMA;Orgien
    push __data_tarea_4_DF      ;Destino
    call td3_memcopy
    add esp, 4*3 
    
;Copio tarea 4 rodata
    push __size_rodata_tarea_4    ;Tamaño 
    push __init_rodata_tarea_4_LMA;Orgien
    push __rodata_tarea_4_DF      ;Destino
    call td3_memcopy
    add esp, 4*3 
    
    mov eax, DIR_PAG_1
    mov cr3,eax                ;Apuntar a directorio de paginas.
    mov eax,cr4                ;Activar el bit Page Size Enable (bit 4 de CR4) para habilitar las paginas grandes.
    or al, 0x10
    mov cr4,eax
    mov eax,cr0                ;Activar paginacion encendiendo el bit 31 de CR0
    or eax, 0x80000000
    mov cr0,eax
    
;==============================================;
;Inicialización de valores de tareas en la pila;
;==============================================;
    mov eax, DIR_PAG_1              ;Inicializo pila de Tarea 1
    mov cr3, eax
    mov esp, __init_stack_kernel_tarea_1
    push ds_sel_pl3                 ;SS3
    push __init_stack_tarea_1       ;ESP3
    push 0x200                      ;EFLAGS
    push cs_sel_pl3                 ;CS PL=3
    push __text_tarea_1             ;EIP
    mov ebp, __init_stack_tarea_1   ;Antes de salvar los Reg. de uso general, EBP=ESP.
    xor eax, eax                    ;El resto de los registros los inicializo a 0.
    xor ebx, ebx
    xor ecx, ecx
    xor edx, edx
    xor esi, esi
    xor edi, edi
    pushad                              ;Salvo registros de uso general
    mov ecx, 4
salvarRegistrosSegT1:                   ;Registro de segmento DS, ES, FS y GS en PL=0.
    push ds_sel_pl3
    loop salvarRegistrosSegT1
    push ds_sel                         ;Guardo en la pila SS0 de la TSS.
    push __init_stack_kernel_tarea_1    ;Guardo en la pila ESP0 de la TSS.
    mov word[contexto_pila+8], ds_sel   ;Salvo SS PL=0 actual en el array.
    mov dword[contexto_pila+8+2], esp   ;Salvo ESP PL=0 actual en el array.
    
    ;Inicializo pila de Tarea 2
    mov eax, DIR_PAG_2
    mov cr3, eax
    mov esp, __init_stack_kernel_tarea_2
    push ds_sel_pl3                 ;SS3
    push __init_stack_tarea_2       ;ESP3
    push 0x200                      ;EFLAGS
    push cs_sel_pl3                 ;CS PL=3
    push __text_tarea_2             ;EIP
    mov ebp, __init_stack_tarea_2   ;Antes de salvar los Reg. de uso general, EBP=ESP.
    xor eax, eax                    ;El resto de los registros los inicializo a 0.
    xor ebx, ebx
    xor ecx, ecx
    xor edx, edx
    xor esi, esi
    xor edi, edi
    pushad                              ;Salvo registros de uso general
    mov ecx, 4
salvarRegistrosSegT2:                   ;Registro de segmento DS, ES, FS y GS en PL=0.
    push ds_sel_pl3
    loop salvarRegistrosSegT2
    push ds_sel                         ;Guardo en la pila SS0 de la TSS.
    push __init_stack_kernel_tarea_2    ;Guardo en la pila ESP0 de la TSS.
    mov word[contexto_pila+8*2], ds_sel ;Salvo SS PL=0 actual en el array.
    mov dword[contexto_pila+8*2+2], esp ;Salvo ESP PL=0 actual en el array.
    
    ;Inicializo pila de Tarea 3
    mov eax, DIR_PAG_3
    mov cr3, eax
    mov esp, __init_stack_kernel_tarea_3
    push ds_sel_pl3                 ;SS3
    push __init_stack_tarea_3       ;ESP3
    push 0x200                      ;EFLAGS
    push cs_sel_pl3                 ;CS PL=3
    push __text_tarea_3             ;EIP
    mov ebp, __init_stack_tarea_3   ;Antes de salvar los Reg. de uso general, EBP=ESP.
    xor eax, eax                    ;El resto de los registros los inicializo a 0.
    xor ebx, ebx
    xor ecx, ecx
    xor edx, edx
    xor esi, esi
    xor edi, edi
    pushad                              ;Salvo registros de uso general
    mov ecx, 4
salvarRegistrosSegT3:                   ;Registro de segmento DS, ES, FS y GS en PL=3.
    push ds_sel_pl3
    loop salvarRegistrosSegT3
    push ds_sel                         ;Guardo en la pila SS0 de la TSS.
    push __init_stack_kernel_tarea_3    ;Guardo en la pila ESP0 de la TSS.
    mov word[contexto_pila+8*3], ds_sel ;Salvo SS PL=0 actual en el array.
    mov dword[contexto_pila+8*3+2], esp ;Salvo ESP PL=0 actual en el array.
    
    ;Inicializo pila de Tarea 4
    mov eax, DIR_PAG_4
    mov cr3, eax
    mov esp, __init_stack_kernel_tarea_4
    push 0x200                      ;EFLAGS inicial
    push cs_sel                     ;CS PL=0
    push __text_tarea_4             ;EIP
    mov ebp, __init_stack_kernel_tarea_4   ;Antes de salvar los Reg. de uso general, EBP=ESP(actual en PL=0).
    xor eax, eax                    ;El resto de los registros los inicializo a 0.
    xor ebx, ebx
    xor ecx, ecx
    xor edx, edx
    xor esi, esi
    xor edi, edi
    pushad                          ;Salvo registros de uso general
    mov ecx, 5
salvarRegistrosSegT4:                   ;Registro de segmento DS, ES, FS, GS y la SS0 de la TSS
    push ds_sel                         ;Selector de datos en RPL=0
    loop salvarRegistrosSegT4
    push __init_stack_kernel_tarea_4    ;Guardo en la pila de PL0 el ESP0 de la TSS.
    mov word[contexto_pila+8*4], ds_sel ;Salvo SS0 actual en el array.
    mov dword[contexto_pila+8*4+2], esp ;Salvo ESP0 actual en el array.
    
    mov esp, __init_stack_kernel
    mov ebp, esp
    
;Cargo direccionenes de manejadores en las tablas IDT y GDT
    mov eax, TSS_inicial
    mov esi, sel_TSS_inicial
    mov [esi+2], ax
    shr eax, 16
    mov [esi+4], al
    mov [esi+7], ah
    
    mov eax, td3_halt
    mov esi, sel_sysCall_halt
    call init_handler_IDT       ;EAX: handler, ESI: base
    
    mov eax, td3_read
    mov esi, sel_sysCall_read
    call init_handler_IDT       ;EAX: handler, ESI: base
    
    mov eax, td3_print
    mov esi, sel_sysCall_print
    call init_handler_IDT       ;EAX: handler, ESI: base

    mov eax, cantidadTablaDigitos
    mov esi, sel_sysCall_cantidadTablaDigitos
    call init_handler_IDT       ;EAX: handler, ESI: base
    
    mov eax, readTablaDigitos
    mov esi, sel_sysCall_readTablaDigitos
    call init_handler_IDT       ;EAX: handler, ESI: base
    
    mov eax, ImprimirPantalla
    mov esi, sel_sysCall_imprimirPantalla
    call init_handler_IDT       ;EAX: handler, ESI: base
    
    mov eax, read_Dir
    mov esi, sel_sysCall_read_dir
    call init_handler_IDT       ;EAX: handler, ESI: base
    
    mov eax, ManejadorExc6
    mov esi, IDT_exc6
    call init_handler_IDT       ;EAX: handler, ESI: base
    
    mov eax, ManejadorExc7
    mov esi, IDT_exc7
    call init_handler_IDT       ;EAX: handler, ESI: base
    
    mov eax, ManejadorExc8
    mov esi, IDT_exc8
    call init_handler_IDT       ;EAX: handler, ESI: base
    
    mov eax, ManejadorExc12
    mov esi, IDT_exc12
    call init_handler_IDT       ;EAX: handler, ESI: base
    
    mov eax, ManejadorExc14
    mov esi, IDT_exc14
    call init_handler_IDT       ;EAX: handler, ESI: base
    
    mov eax, ManejadorExc17
    mov esi, IDT_exc17
    call init_handler_IDT       ;EAX: handler, ESI: base
    
    mov eax, ManejadorTeclado
    mov esi, IDT_teclado
    call init_handler_IDT       ;EAX: handler, ESI: base
    
    mov eax, ManejadorTimer
    mov esi, IDT_timer
    call init_handler_IDT       ;EAX: handler, ESI: base
    
    ;Inicializo TSS_inicial
    mov dword[TSS_inicial+0x04], __init_stack_kernel_tarea_4   ;ESP0
    mov dword[TSS_inicial+0x08], ds_sel                 ;SS0
    mov dword[TSS_inicial+0x20], __text_tarea_4         ;EIP
    mov dword[TSS_inicial+0x24], 0x200                  ;EFLAGS
    mov dword[TSS_inicial+0x48], ds_sel_pl3             ;ES
    mov dword[TSS_inicial+0x50], ds_sel_pl3             ;SS
    mov dword[TSS_inicial+0x54], ds_sel_pl3             ;DS
    mov dword[TSS_inicial+0x58], ds_sel_pl3             ;FS
    mov dword[TSS_inicial+0x5C], ds_sel_pl3             ;GS
    mov dword[TSS_inicial+0x60], 0                      ;LDTR
    
    LGDT [imagen_GDT]   ;Cargo la tabla GDT
    LIDT [IDTR]         ;Cargo la tabla IDT
    
    mov ax, ds_sel          ;Se inicializa los registros de segmento
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax
    xor ax, ax
    lldt ax                 ;Cargo LDT con cero dado que no se usa
    mov ax, sel_TSS_inicial
    ltr ax                  ;Cargo la TSS de la tarea inicial en TR.
    
    ;Habilito la funcionalidad de TS en SSE con CR4.9 y CR4.10
    mov eax, cr4
    or eax, 0x600
    mov cr4, eax
    
    STI                 ;Habilitar interrupciones
    
    jmp __kernel
    
;*****************************************************************************;    
;void *td3_memcopy(void *destino, const void *origen, unsigned int num_bytes)
;Parametros: *destino: edi; *origen: esi; num_bytes: ecx
td3_memcopy:
    push ebp        ;PRÓLOGO
    mov ebp, esp
    push edi        ;CUERPO
    push esi
    push ecx
    
    mov edi, [ebp+8]    ;destino
    mov esi, [ebp+8+4]  ;origen
    mov ecx, [ebp+8+4*2];num_bytes
    cld
    rep cs movsb   ;Efectuar copia mov [ES:DI], [DS:SI]
    
    pop ecx
    pop esi
    pop edi
    
    mov esp, ebp    ;EPÍLOGO
    pop ebp
    ret            ;Fin de la subrutina.
end_td3_memcopy:
        
;*****************************************************************************;
;Carga las tablas de paginas.
;EDI: destino ; EBX: direccion fisica + atributo; EAX: cantidad en bytes
cargar_tabla:    
    cmp eax, 0              ;Se verifica que eax sea distinto de 0B.
    jne .continuar
    inc eax                 ;Si es igual a 0, se le suma 1B para asignar una pagina.
.continuar:
    mov edx, 0              ;Se determina la cantidad de paginas a utilizar.
    add eax, 0xFFF          ;Cantidad de Bytes + una compensación para determinar la cantidad de páginas necesarias.
    mov ecx, 0x1000         ;Divido entre 4kB, corresponde al tamaño de las páginas.
    div ecx
    mov ecx, eax            ;Tamaño (max. 0x100)
.carga:
    mov dword[edi], ebx     ;Cargo contenido en tabla
    add edi, 4              ;Siguiente entrada
    add ebx, 0x1000         ;Se incrementa 4kB para la siguiente base a pagina
    loop .carga
    
    ret
    
;*****************************************************************************;
;Inicializar handler de la IDT
;EAX: handler, ESI: base
init_handler_IDT:
    mov [esi], ax
    shr eax, 16
    mov [esi+6],ax
    ret
fin_init_handler_IDT:
    
;*****************************************************************************;
;Carga las tablas de paginas correspondiente al kernel.
;ESI: directorio de paginas
kernel_paginacion:
;Directorios de paginas
    lea eax, [esi+0x1000+SUP+WE+P]                  ;Direccion de la tabla de paginas.
    mov dword[esi], eax                             ;Tablas del sistema y paginacion, Video, ISR
    mov dword[esi+4*0x004], eax                     ;Datos, tabla de digitos, kernel
    mov dword[esi+4*0x0BF], eax                     ;Pila kernel
    mov dword[esi+0x3FF*4], 0xFFC00000+PS+SUP+WE+P  ;Entrada para el directorio de pagina correspondiente a la ROM.
    
;Tabla de paginas
    ;Tablas del sistema
    mov eax, __size_sys_tables
    lea edi, [esi+0x1000+4*0x000]
    mov ebx, __sys_tables_DF+SUP+WE+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Tablas de paginas
    lea edi, [esi+0x1000+4*0x010]
    mov ebx, __page_tables_DF+SUP+WE+P
    mov eax, SIZE_PAGE_TABLES
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Video
    mov dword[esi+0x1000+4*0x020], __video_DF+SUP+WE+P
    
    ;ISR
    mov eax, __size_rutinas
    lea edi, [esi+0x1000+4*0x100]
    mov ebx, __rutinas_DF+SUP+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Datos
    mov eax, __size_datos
    lea edi, [esi+0x1000+4*0x200]
    mov ebx, __datos_DF+SUP+WE+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes

    ;Tabla de digitos
    mov dword[esi+0x1000+4*0x210], __tabla_digitos_DF+SUP+WE+P
    
    ;Kernel
    mov eax, __size_kernel
    lea edi, [esi+0x1000+4*0x220]
    mov ebx, __kernel_DF+SUP+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
    
    ;Kernel Stack
    mov eax, __size_stack_kernel+1
    lea edi, [esi+0x1000+4*0x3F8]
    mov ebx, __stack_kernel_DF+SUP+WE+P
    call cargar_tabla       ;EDI: destino; EBX: direccion fisica + atributo; EAX: cantidad en bytes
fin_kernel_paginacion:
    ret
    
;*****************************************************************************;
    section .reset PROGBITS
    use16
Reset:
    jmp dword Inicio   ; Esta instrucción debe estar
                       ; en el offset 0xFFF0.	
                    
    ; Relleno hasta el final de la ROM
    times 16-($-Reset) db 0
