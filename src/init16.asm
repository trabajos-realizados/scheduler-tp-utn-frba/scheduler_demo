USE16

inicio_ROM equ 0xFFFF0000  ;Son los 64kB de la memoria direccionable

Inicio:
    jmp inicio_programa

;Tabla de descritores. 
GDT:    dq  0       ;Primeros 8B van vacios
cs_sel  equ $-GDT
;Descriptores. Limite = 0xFFFFFFFF, Base = 0x00000000
    dw  0xFFFF      ;Limite 15-0
    dw  0           ;Base 15-0
    db  0           ;Base 23-16
    db  0x9A        ;Derechos de acceso: P=1; DPL=0b00; b4=1; b3=1(codigo); b2=0 (no conforme); R=1; b0=0.
    db  0xCF        ;G=1; D=1 (32-bits); limite 19-16
    db  0           ;Base 31-24
ds_sel  equ $-GDT   ;Descriptor de datos. limite 4GB y base 0
    dw  0xFFFF      ;Limite 15-0
    dw  0x0000      ;Base 15-0
    db  0x00        ;Base 23-16
    db  0x92        ;Derechos de acceso: P=1; DPL=0b00; b4=1; b3=0(datos); b2=0 (expansion); W=1; b0=0.
    db  0xCF        ;G=1; D=1 (32-bits); limite 19-16
    db  0x00        ;Base 31-24
long_GDT    equ $-GDT

imagen_GDT:
    dw  long_GDT-1          ;Limite
    dd  (inicio_ROM + GDT)  ;Base

inicio_programa:
    %include "./inc/init_pci.inc"
    cli                         ;Deshabilito interrupciones
    o32 LGDT    [cs:imagen_GDT] ;Cargo el registro GDTR con la tabla de descritores
    mov eax, cr0
    or  eax, 1
    mov cr0, eax                ;Habilito el modo protegido
    
    jmp dword   cs_sel:inicio_ROM+protegido

    BITS 32

protegido:
    mov eax, ds_sel  ;Cargo el segmento de datos en Data y Stack segment
    mov ds, eax
    mov es, eax
    mov ss, eax
;**********************************************************;
