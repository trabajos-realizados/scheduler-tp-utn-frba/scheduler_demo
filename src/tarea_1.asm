;*****************************************************************************;
;                               TAREA 1                                       ;
;*****************************************************************************;

;Funciones C

;Funicones ASM
extern sel_sysCall_imprimirPantalla
extern sel_sysCall_cantidadTablaDigitos
extern sel_sysCall_readTablaDigitos
extern sel_sysCall_read_dir
extern sel_sysCall_halt

;Definiciones
SIZE_STACK_TAREA_1  equ 3*1024     ;Tamano en bytes

;*****************************************************************************;
section .bss_tarea_1 NOBITS
promedio    resb    8       ;Int de 64 bits
string      resb    16+1    ;String para guardar un numero de 64 bits

;*****************************************************************************;
section .data_tarea_1 PROGBITS

;*****************************************************************************;
section .rodata_tarea_1 PROGBITS
mensaje     db  'Tarea 1:', 0

;*****************************************************************************;
section .stack_tarea_1 NOBITS
stack_tarea_1   resb    SIZE_STACK_TAREA_1

;*****************************************************************************;
section .text_tarea_1 PROGBITS
Tarea_1:
    ;SE IMPRIME EL MENSAJE INDICANDO LA TAREA
    ;ImprimirPantalla(int8 x, int8 y, char *str, int atributos);
    ;Atributos: 1B [blink, R, G, B(fondo), brillo, R, G, B(frente)]       ;
    push 0x0F           ;atributos
    push mensaje        ;*str
    push 0              ;y->0
    push 54             ;x->54
    call sel_sysCall_imprimirPantalla:0 ;Se imprime en pantalla el valor leido. 
    add esp, 4*4
    
    ;uint16_t sel_sysCall_cantidadTablaDigitos()
    call sel_sysCall_cantidadTablaDigitos:0 ;Retorna en AX la cantidad de datos en Tabla de Dígitos.
    cmp ax, 0                   ;Si no hay datos, termino con la tarea.
    je fin_tarea_1
    
    mov dword[promedio], 0      ;Restauro promedio.
    mov dword[promedio+4], 0
    movzx ebx, ax               ;El valor de AX lo paso a EBX.
    mov eax, 0                  ;Inicializo EAX, EDX y ECX para el cálculo del promedio.
    mov edx, 0
    mov ecx, 0

calculo_promedio:
    ;uint64_t readTablaDigitos(uint32_t indice). Leo un valor de la tabla indicado por el indice.
    push ecx                                ;Indice
    call sel_sysCall_readTablaDigitos:0     ;Valor en EDX:EAX
    add esp, 4                              ;Balance de pila
    add dword[promedio], eax                ;Primeros 32 bits. Sumatoria y se guarda en "promedio".
    adc dword[promedio+4], edx              ;Ultimos 32 bits
    inc ecx
    cmp ecx, ebx                            ;Si se llego a leer toda la tabla: if( ECX == EBX)
    jne calculo_promedio                    ;si no: se siguen leyendo datos.
    mov eax, dword[promedio]
    mov edx, 0                              ;de lo contrario, se procede a dividir, es decir, calcular el promedio
    div ecx                                 ;Resultado dado en EAX      
    mov dword[promedio], eax
    mov dword[promedio+4], 0
    
    ;void itoa(int num, char *str)
    push string             ;*string
    push dword[promedio]    ;num
    call itoa
    add esp,4*2             ;Equilibro pila
    
    ;ImprimirPantalla(int8 x, int8 y, char *str, int atributos);
    ;Atributos: 1B [blink, R, G, B(fondo), brillo, R, G, B(frente)]       ;
    push 0x0F           ;atributos
    push string         ;*str
    push 0              ;y->0
    push 63             ;x->63
    call sel_sysCall_imprimirPantalla:0 ;Se imprime en pantalla el valor leido. 
    add esp, 4*4
    
lectura_RAM:
    mov dword[string], 0                ;Limpio el string
    mov word[string], "--"              ;Inicializo el string con "--", se imprime en caso de que no se pueda leer el puntero.
    ;Intento de leer en la dirreción dada por el resultado "promedio"(EAX)
    cmp dword[promedio], 0x20000000     ;Se compueba que no supere los 512MB de la RAM
    jae imprimir_lectura_RAM            ;Si la dirección lo supera imprimo "--".
    mov word[string], "  "              ;Limpio el string
    
    ;ImprimirPantalla(int8 x, int8 y, char *str, int atributos);
    ;Atributos: 1B [blink, R, G, B(fondo), brillo, R, G, B(frente)]       ;
    push 0x0F           ;atributos
    push string         ;*str
    push 1              ;y->1
    push 78             ;x->78
    call sel_sysCall_imprimirPantalla:0 ;Limpio la pantalla
    add esp, 4*4
    
    push dword[promedio]                ;Leo el byte en la direción apuntada.
    ;uint16_t read_Dir(uint32_t base). Valor leido en AX.
    call sel_sysCall_read_dir:0         ;Intento leer la direccion obtenida.
    add esp, 4                          ;Balance de pila.
    
    ;void itoa(int num, char *str)
    push string         ;*string
    push eax            ;num
    call itoa
    add esp,4*2         ;Equilibro pila
imprimir_lectura_RAM:
    ;ImprimirPantalla(int8 x, int8 y, char *str, int atributos);
    ;Atributos: 1B [blink, R, G, B(fondo), brillo, R, G, B(frente)]       ;
    push 0x0F           ;atributos
    push string         ;*str
    push 1              ;y->1
    push 78             ;x->78
    call sel_sysCall_imprimirPantalla:0
    add esp, 4*4

fin_tarea_1:
    call sel_sysCall_halt:0
    jmp Tarea_1
;=============================================================================;
;Sección Include.
include_tarea_1:
    %include "./inc/stdio.inc"
