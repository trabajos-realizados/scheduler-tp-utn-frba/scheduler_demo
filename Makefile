AC=nasm
CC = gcc
CFLAGS=-f bin

INIT16 = init16
INIT32 = init32
MAIN = main
LINKER = linker.ld
NAME_OUT = mi_bios

DEBUG = bochs.cfg

OBJ= init32.o main.o functions.o sys_tables.o buffer.o tarea_1.o tarea_2.o tarea_3.o tarea_4.o

all: $(NAME_OUT)
	@echo "###Finalizado"
	
$(NAME_OUT): $(OBJ)
	@echo "###Enlazando"
	ld -z max-page-size=0x1000 -m elf_i386 -T ./$(LINKER) $(OBJ) -o $@.o
	ld -z max-page-size=0x1000 --oformat=binary -m elf_i386 -T ./$(LINKER) $(OBJ) -o ./bin/$@.bin
	
%.o: ./src/%.c
	$(CC) -fno-stack-protector -fno-pie -m32 $< -c -o $@
	
init32.o: ./src/$(INIT32).asm ./src/$(INIT16).asm
	@echo "###Compilando..."
	mkdir -p bin
	$(AC) $(CFLAGS) ./src/$(INIT16).asm -o ./bin/$(INIT16).bin -l $(INIT16).lst
	$(AC) ./src/$(INIT32).asm -f elf32 -o $@ -l init32.lst
	
main.o: ./src/main.asm
	$(AC) $< -f elf32 -o $@ -l main.lst
	
%.o: ./src/%.asm
	$(AC) $< -f elf32 -o $@
	
run:
	make
	@echo "DEBUG BOCHS..."
	bochs -qf ./sub/$(DEBUG)
	
section:
	readelf -s $(NAME_OUT).o
	
clone:
	@echo "Clonando repositorio..."
	git clone $(LINK_REPO)
upload:
	@echo "Subiendo al GitLab..."
	git add .
	git commit -m "$(MENSAJE_GIT)"
	git push
	
help:
	@echo "REGLAS MAKE"
	@echo "all:	Compila todos los archivos dentro de la carpeta make."
	@echo "run:	Ejecuta el debug bochs con la configuración establecia en bochs.cfg"
	@echo "section:	Muestra las secciones resultantes de la compilación"
	@echo "clean:	Elimina los archivos generados por otras reglas."
	
clean:
	@echo "Eliminando archivos"
	rm -r bin
	rm -f *.lst *.bin *.o *.elf bochsout.txt *.ini
	@echo "Listo!"	
